import { IsNotEmpty,MinLength,IsPositiveInt} from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(8)
  name: string;

  @IsNotEmpty()
  @IsPositiveInt()
  price: number;
}